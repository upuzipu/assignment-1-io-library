section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    ret 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp   byte [rdi+rax], 0
        je    .end
        inc   rax
        jmp   .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax 
    mov rsi, rdi 
    mov rdi, 1 
    mov rax, 1 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rdx, 1     
    mov rsi, rsp 
    pop rdi
    mov rax, 1      
    mov rdi, 1      
    syscall         
    ret
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10
    xor r9, r9
    .loop:
        mov sil, [rdi+r9]
        cmp sil, 0
        je .end
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        mul r8
        sub sil, '0'
        add rax, rsi
        inc r9
        jmp .loop
    .err:
        mov rdx, 0
        ret
    .end:
        mov rdx, r9
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .pr_pos
        .pr_neg:
    neg rdi
    mov r10, rdi
    mov rdi, 0x2D
    call print_char
    mov rdi, r10
        .pr_pos:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx 
    xor r8, r8 
    xor r9, r9
    .loop: 
        mov r8b, byte[rdi+rcx] 
        mov r9b, byte[rsi+rcx] 
        cmp r8b,r9b 
        jne .fail 
        cmp r8b,0
        je .success 
        inc rcx 
        jmp .loop
    
    .fail:
        mov rax, 1   
        ret
    .success:
        mov rax,0 
        ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop rcx
        cmp rax, 0
        je .end
        cmp rax, '	'
        je .skip1
        cmp rax, '	'
        je .skip1
        cmp rax, '\n'
        je .skip1
        mov [rdi, rcx], rax
        inc rcx
        cmp rcx, rsi
        jge .fail
        jmp .loop
    
    .skip1:
        cmp rcx, 0
        je .loop
        jmp .end
    
    .fail:
        xor rax, rax
        xor rdx, rdx
        ret
    
    .end:
        xor rax, rax
        mov [rdi+rcx], rax
        mov rax, rdi
        mov rdx, rcx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
    .loop:
        mov dl, byte[rdi+r10]
        cmp rdx, 0x30 
        jl .end
        cmp rdx, 0x39
        jg .end
        sub rdx, 0x30
        push rdx
        add rax, rdx
        inc r10 
        jmp .loop
    
    .end:
        mov rdx, r10
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 0x2D
    jnz .unsigned
    inc rdi
    call parse_int
    neg rax
    inc rdx
    ret
    .unsigned:
        call parse_uint
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx 
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi
    mov r8, rax
    cmp rdx, r8
    jl .fail
    .loop:
        cmp rcx, r8
        jg .end
        mov r10, [rdi+rcx]
        inc rcx
        jmp .loop
    
    .fail:
        mov rax, 0
        ret
    
    .end:
        mov rax, r8
        ret
